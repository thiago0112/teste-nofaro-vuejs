import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost/api/v1',
});

export default client;
