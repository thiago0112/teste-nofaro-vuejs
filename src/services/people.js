import http from './http';
import { unmap, map } from './../mappers/people';

/**
 * Get all people
 * @param {object} filters
 */
export const getAll = (search) => {
  const params = {};

  if (search) {
    params.name = search;
  }

  if (search && search.indexOf('@') >= 0) {
    params.email = search;
  }

  return http.get('/people', { params })
    .then(result => Object.assign(result, {
      data: {
        people: result.data.people.map(person => map(person)),
      },
    }));
};

/**
 * Create person
 * @param {object} data
 */
export const create = data => http.post('/people', unmap(data));

/**
 * Update person
 * @param {number} id
 * @param {object} data
 */
export const update = (id, data) => http.put(`/people/${id}`, unmap(data));

/**
 * Remove person
 * @param {number} id
 * @param {object} data
 */
export const remove = id => http.delete(`/people/${id}`);
