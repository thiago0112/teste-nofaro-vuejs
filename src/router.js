import Vue from 'vue';
import Router from 'vue-router';
import PeopleContainer from './../src/containers/PeopleContainer.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'people',
      component: PeopleContainer,
    }
  ],
});
