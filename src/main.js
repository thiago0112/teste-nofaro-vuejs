import Vue from 'vue';
import VeeValidate from 'vee-validate';

import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.use(VeeValidate);

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app');
