/* eslint-disable camelcase */

/**
 * Unmap application data to external data
 * @param {object} data
 */
export const unmap = ({ phoneNumber, ...restData }) => {
  if (!phoneNumber) {
    return Object.assign({}, restData);
  }

  const [areaCode, realPhoneNumber] = phoneNumber.split('-');

  return Object.assign(restData, {
    area_code: parseInt(areaCode, 0),
    phone_number: parseInt(realPhoneNumber, 0),
  });
};

/**
 * Map external data to application data
 * @param {Object} data
 */
export const map = ({ area_code, phone_number, ...restData }) => ({
  ...restData,
  phoneNumber: area_code && phone_number ? `${area_code}-${phone_number}` : '-'
});
